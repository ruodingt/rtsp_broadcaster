For knowledge MVP3

## Option 1
### Components:

|Document Ranker|Query Expansion|Disambiguation|
| --- | --- | --- |
|ElasticSearch|Positional Relevance Feedback|?|

#### Search enhancement
Indexing word pieces instead of normalised token 

#### 

#####Previous approach:
Use local tf-idf built from titles to find keywords/concepts set
that could cover all document, so there is no explicit 
document aggregation here. It simply use keyword to represent 
a certain document subset.

##### enhancement
Key of disambiguation:
high-level representation of aggregated document group
So it is about 1.aggregation, and 2.representation
And 3.there is a biggest enemy - query drifting: the representation will
inevitably get involved into the the next round of query refinement. If 
the representation is not good enough, it will lead user to another irrelevant topic.

Representation should contains two level:
Human readable: s sequence of token
Lucene syntax representation for ES

An advanced version should roughly work like that:
```
U: solid blue
AI: ? (Generative that produce token sequences)
    Are you trying to search:
    solid blue led
    solid blue wifi
```
Disambiguation should also encode _positional information_ - 
the distance bwt disambiguation candidate words vs. observed words.

To prevent query drifting the secret might hidden in side positional relevance feed back

what if we break down positional relevance feedback for disambiguation options - 
**Good idea**!!! Positional relevance feedback inside the blackbox, we should 
gut it!

###Blockers

#### automatically generate artefact:

Offline: nearly impossible

Online: Ranker -\> MRC

##### Online artifact:

Let user preview the content, but what to preview: 
Preview the high-conf region!
Toggle down the low confidence paragraph
Toggle up the high confidence paragraph
highlight the keywords!

Chat bot is not the best interface for IR

MRC - longform QA