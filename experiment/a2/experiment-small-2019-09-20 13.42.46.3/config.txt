{
    "identification_test_sample_images_pp": 5,
    "identification_distraction_size": 100,
    "data_set": {
        "num_baseline": {
            "min_len": 7,
            "num_test": 5,
            "num_uids": 500,
            "data_dir": "data/cfp-dataset/Data/Images/",
            "num_baseline": 2
        }
    },
    "test_size": 50,
    "verification_negative_sample_size": 20
}