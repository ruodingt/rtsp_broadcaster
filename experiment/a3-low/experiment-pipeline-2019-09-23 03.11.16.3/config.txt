{
    "identification_test_sample_images_pp": 5,
    "identification_distraction_size": 3,
    "data_set": {
        "num_baseline": {
            "min_len": 7,
            "num_uids": 500,
            "data_dir": "data/cfp-lower-res/Data/Images/",
            "num_baseline": 2,
            "num_test": 5
        }
    },
    "test_size": 3,
    "verification_negative_sample_size": 5
}