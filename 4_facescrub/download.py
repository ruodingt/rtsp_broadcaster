#!/usr/bin/env python2.7
# coding: utf-8
import hashlib
import http.client
import imghdr
import os
import ssl
import urllib.error
import urllib.parse
import urllib.request
from collections import namedtuple, defaultdict
from concurrent.futures import ThreadPoolExecutor
from os.path import join, exists

import cv2

TaskUnit = namedtuple("TaskUnit", "name,url,bbox")

files = ['facescrub_actors.txt', 'facescrub_actresses.txt']
RESULT_ROOT = './download5'
if not exists(RESULT_ROOT):
    os.mkdir(RESULT_ROOT)


def download(subtasks):
    """
        download from urls into folder names using wget
    """

    assert list(map(lambda x: x.name == subtasks[0].name, subtasks))
    directory = join(RESULT_ROOT, subtasks[0].name.decode('utf8', 'ignore'))
    if not exists(directory):
        os.mkdir(directory)
    print("downloading to: ", directory)

    fii = 0
    print('\ndownloading images for', subtasks[0].name, '\n--------------------------------------------')
    for t in subtasks:
        # t = subtasks[i]
        # _t_url = 'http://upload.wikimedia.org/wikipedia/commons/a/a7/Charlie_Sheen_2012.jpg'.encode()
        # t = Record(name='test', url=_t_url, bbox=[0,0,10,10])

        o_filename = urllib.parse.urlsplit(url=t.url.decode('utf8', 'ignore')).path
        _, file_extension = os.path.splitext(o_filename)

        file_extension = file_extension.lower()
        if file_extension == '.jpg':
            fname = hashlib.sha1(t.url).hexdigest() + file_extension
            file_dst_path = join(directory, fname)
            # print("downloading", file_dst_path)
            if exists(file_dst_path):
                # print("already downloaded, skipping...")
                continue
            else:
                try:
                    urllib.request.urlretrieve(t.url.decode('utf8', 'ignore'), file_dst_path)
                    # curl_cmd = pycurl.Curl()
                except (urllib.error.HTTPError, urllib.error.URLError,
                        http.client.RemoteDisconnected, ssl.CertificateError,
                        http.client.BadStatusLine, ConnectionResetError) as err:
                    pass
                except Exception:
                    pass

            # img = None
            if exists(file_dst_path):
                try:
                    img = cv2.imread(file_dst_path)
                    if img is not None:
                        is_right_size = img.shape[0] < 4096 and img.shape[1] < 4096
                    else:
                        is_right_size = False
                    im_format = imghdr.what(file_dst_path)

                    if im_format is not None:
                        is_verified = (im_format.lower() == 'jpeg') and is_right_size
                    else:
                        is_verified = False
                    # print(im_format)
                except OSError:
                    is_verified = False
                    pass

                if not is_verified:
                    print("remove ", file_dst_path, ' -- ', t.url)
                    os.remove(file_dst_path)
                else:
                    # crop_face_and_save(t, img, directory, fname)
                    fii += 1
                    print('downloaded..', t.name, fii)
                    # print(fii)
        if fii > 50:
            break


def crop_face_and_save(t, img, directory, fname):
    # get face
    face_directory = join(directory, 'face')
    if not exists(face_directory):
        os.mkdir(face_directory)

    face_path = join(face_directory, fname)
    face = img[t.bbox[1]:t.bbox[3], t.bbox[0]:t.bbox[2]]
    cv2.imwrite(face_path, face)
    # write bbox to file
    with open(join(directory, '_bboxes.txt'), 'a') as fd:
        bbox_str = ','.join([str(_) for _ in t.bbox])
        fd.write('%s %s\n' % (fname, bbox_str))


if __name__ == '__main__':
    for f in files:
        with open(f, 'r', encoding='utf8') as fd:
            # strip first line
            fd.readline()

            records = defaultdict(list)
            for line in fd.readlines():
                components = line.split('\t')
                assert (len(components) == 6)
                name = components[0].replace(' ', '_').lower().encode()
                url = components[3].encode()
                bbox = [int(_) for _ in components[4].split(',')]
                r = TaskUnit(name=name, url=url, bbox=bbox)
                # if r.name not in records:
                #     records[r.name] = []
                records[r.name].append(r)

        tasks = list(records.values())

        # pool_size = multiprocessing.cpu_count()
        # pool = multiprocessing.Pool(processes=pool_size, maxtasksperchild=2)
        # pool.map(download, tasks)
        # pool.close()
        # pool.join()
        list(map(lambda sub: download(sub), tasks))

        executor = ThreadPoolExecutor(10)

        executor.map(lambda sub: download(sub), tasks)

        # future = executor.submit(task, ("Completed"))
        # print(future.done())
        # sleep(2)
        # print(future.done())
        # print(future.result())
