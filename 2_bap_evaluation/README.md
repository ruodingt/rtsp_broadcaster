This dir is for evaluate BAP

use cfpw data set
download dataset at:
http://www.cfpw.io/


Config you experiment before running `python3 main_eval.py`

```python

def exp_run(data_set='cfp',
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=4,
            test_size=200,
            verification_negative_identity_sample=50,
            identification_distraction_size=50,
            reset=False,
            mode='vd',
            title='NA',
            experiment_set='frontal'):
    """

    :param title: customise your experiment folder (it will contain the experiment results)
    :param experiment_set: use 'frontal' as default value.
    :param data_set: choose from 'cfp-lower-res', 'cfp'
    :param num_baseline_per_id: how many image to enroll per identity
    :param num_test_per_id: how many image per identity are used for testing
    :param min_sample_per_id: normally should be （num_test_per_id+num_baseline_per_id） to ensure consistency
    :param test_size: total num of people to be enrolled into the system
    :param verification_negative_identity_sample: num of distraction identity (not enrolled into the system)
    :param identification_distraction_size: num of distraction identity (not enrolled into the system)
    :param reset: reset database. default should be true
    :param mode: 'v', 'd', 'vd' for verification only, identification only, both
    :return:
    """
    ...
```


dataset path is set as magic string. you can change the path via searching for `DataSetLookUp` class