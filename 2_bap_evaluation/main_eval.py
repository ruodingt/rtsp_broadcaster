import pickle
import time
from datetime import datetime
import itertools
import json
import random
import re

import cv2
import numpy
import requests
from enum import Enum
import os
import urllib.parse
import sklearn.metrics
import matplotlib.pyplot as plt
from sklearn.metrics import average_precision_score, roc_auc_score


class ContentType(Enum):
    APP_MS_EXCEL = 'application/vnd.ms-excel'
    TEXT_PLAIN = 'text/plain'
    APP_JSON = 'application/json'
    IMG_PNG = 'image/png'
    IMG_JPEG = 'image/jpeg'


class FormDataPart:
    def __init__(self, name, content_type: ContentType, content=None, filename=None):
        self.name = name
        self.content = content
        self.content_type = content_type
        self.filename = filename
        return

    def r(self):
        return self.name, (self.filename, self.content, self.content_type.value, {'Expires': '0'})  # {'Expires': '0'}

    @staticmethod
    def make_json_part(json_dic, name="data"):
        f = FormDataPart(name=name, content_type=ContentType.APP_JSON, content=json_dic, filename=None)
        return f

    @staticmethod
    def make_file_part(name, file_stream, filename, content_type=ContentType.IMG_JPEG):
        f = FormDataPart(name=name, content_type=content_type, content=file_stream, filename=filename)
        return f


class BAPInterface:
    def __init__(self):
        self._user = 'root'
        self._pass = 'Telstra1234'
        _entry = 'bap'

        _host = '10.4.31.11'
        _port = '18000'
        self.url_entry = 'http://{}:{}/{}/'.format(_host, _port, _entry)
        # self.url_entry = 'http://httpbin.org/post/'
        pass

    def get_anything(self, frags):
        _url = self._build_url_w_frags(frags)
        return self._get(_url)

    def delete_anything(self, frags):
        _url = self._build_url_w_frags(frags)
        return self._delete(_url)

    def post_anything(self, frags, fs=None, json_dict=None):
        _url = self._build_url_w_frags(frags)
        resp = self._post(_url, fs, json_dict)
        if resp.status_code not in [201, 204]:
            print("posting to", _url, 'status', resp.status_code)
        return resp

    def _build_url_w_frags(self, frags):
        if isinstance(frags, list):
            _url = urllib.parse.urljoin(self.url_entry, '/'.join(frags))
        elif isinstance(frags, str):
            _url = urllib.parse.urljoin(self.url_entry, frags)
        else:
            raise Exception('WTF, wrong format of url!')
        return _url

    def _get(self, url):
        resp = requests.get(url, auth=(self._user, self._pass))
        return resp

    def _delete(self, url):
        resp = requests.delete(url, auth=(self._user, self._pass))
        return resp

    def _post(self, url, fs, json_dict):
        assert (fs is not None) is not (json_dict is not None), "only one file can be field"
        if fs is not None:
            resp = requests.post(url, files=fs, auth=(self._user, self._pass))
        elif json_dict is not None:
            resp = requests.post(url, json=json_dict, auth=(self._user, self._pass))
        else:
            raise Exception("WTF? Please send right param. POST failed")
        return resp


class BAPIdentityInterface(BAPInterface):
    def __init__(self):
        self.global_uid_set = "0-global-set"
        super().__init__()

    def delete_identity(self, uid):
        resp = self.delete_anything(frags=['identities', str(uid)])
        # print(resp)
        return resp

    def delete_global_identity_set(self):
        resp = self.delete_anything(frags=['identity_sets', self.global_uid_set])
        time.sleep(10)
        return resp

    def list_all_identities(self):
        resp = self.get_anything(frags='identities')
        resp_json = resp.json()
        uid_list = list(map(lambda x: x['name'], resp_json['items']))
        return resp_json, uid_list

    def delete_all_identities(self):
        _, uid_list = self.list_all_identities()
        for uid in uid_list:
            self.delete_identity(uid=uid)

    def create_global_identity_set(self, all_uid, metadata=''):

        json_dict = {
            "name": self.global_uid_set,
            "data": metadata,
            "identity_names": all_uid
        }
        return self.post_anything(frags='identity_sets', json_dict=json_dict), json_dict

    def create_or_update_global_identity_set(self, metadata=''):

        _, all_uid = self.list_all_identities()
        _resp, update_dic = self.create_global_identity_set(all_uid, metadata)
        if _resp.status_code == 409:
            print('Conflict! Update global set instead')
            _resp = self.post_anything(frags=['identity_sets', self.global_uid_set],
                                       json_dict=update_dic)

            print(_resp)
            if _resp.status_code != 204:
                raise Exception('Identity set update failed!')

        # double check:
        check_resp, s_code = self.get_global_identity_set()
        assert len(all_uid) == len(check_resp['identities'])
        print('waiting for global set creation')
        time.sleep(10)
        print('global identity set is good to go!')
        time.sleep(2)
        return _resp

    def get_global_identity_set(self):
        resp = self.get_anything(frags=['identity_sets', self.global_uid_set])
        return resp.json(), resp.status_code

    def create_identity(self, uid: str, display_name: str, img_path_list: [str], pre_dir=None, metadata=""):
        """
        From Gorilla's API doc, the API seems support 2 enrollment images. More enrollment images get
        boost accuracy, perhaps?
        :param pre_dir:
        :param uid:
        :param display_name:
        :param img_path_list: img list, the first image in the list will be used as thumbnail
        :param metadata:
        :return:
        """

        multi_parts = []
        part1_data = {'name': uid,
                      'display_name': display_name,
                      "data": metadata}

        print('creating', display_name, '...')

        fdp_json = FormDataPart.make_json_part(json.dumps(part1_data), name="data").r()

        multi_parts.append(fdp_json)

        for _i, img_path in enumerate(img_path_list):
            filename = os.path.basename(img_path)
            if pre_dir is not None:
                fs = open(os.path.join(pre_dir, img_path), 'rb').read()
            else:
                fs = open(img_path, 'rb').read()

            if _i == 0:
                thumb_part = FormDataPart.make_file_part(name='thumbnail',
                                                         file_stream=fs,
                                                         filename=filename,
                                                         content_type=ContentType.IMG_JPEG)
                multi_parts.append(thumb_part.r())

            im_part = FormDataPart.make_file_part(name='images',
                                                  file_stream=fs,
                                                  filename=filename,
                                                  content_type=ContentType.IMG_JPEG)
            multi_parts.append(im_part.r())

        fs_tp = tuple(multi_parts)
        resp = self.post_anything(frags='identities', fs=fs_tp)

        resp_json = resp.json()

        return fs_tp, resp_json

    def create_verification(self, uid, img_path_list, pre_dir=None, metadata=""):

        multi_parts = []
        part1_data = {'identity_name': uid,
                      "data": metadata}

        fdp_json = FormDataPart.make_json_part(json.dumps(part1_data), name="data").r()

        multi_parts.append(fdp_json)

        for _i, img_path in enumerate(img_path_list):
            filename = os.path.basename(img_path)
            if pre_dir is not None:
                fs = open(os.path.join(pre_dir, img_path), 'rb').read()
            else:
                fs = open(img_path, 'rb').read()

            im_part = FormDataPart.make_file_part(name='images',
                                                  file_stream=fs,
                                                  filename=filename,
                                                  content_type=ContentType.IMG_JPEG)
            multi_parts.append(im_part.r())

        fs_tp = tuple(multi_parts)
        resp = self.post_anything(frags='verifications', fs=fs_tp)
        resp_json = resp.json()
        return fs_tp, resp_json, resp.status_code

    def create_identification(self, uid_set, img_path_list, threshold=0.8, top_n=2,
                              match_only=False,
                              pre_dir=None, metadata=""):

        multi_parts = []
        part1_data = {
            "identity_set_name": uid_set,
            "threshold": threshold,
            "top_n": top_n,
            "match_only": match_only,
            "data": metadata
        }

        fdp_json = FormDataPart.make_json_part(json.dumps(part1_data), name="data").r()

        multi_parts.append(fdp_json)

        for _i, img_path in enumerate(img_path_list):
            filename = os.path.basename(img_path)
            if pre_dir is not None:
                fs = open(os.path.join(pre_dir, img_path), 'rb').read()
            else:
                fs = open(img_path, 'rb').read()

            im_part = FormDataPart.make_file_part(name='images',
                                                  file_stream=fs,
                                                  filename=filename,
                                                  content_type=ContentType.IMG_JPEG)
            multi_parts.append(im_part.r())

        fs_tp = tuple(multi_parts)
        resp = self.post_anything(frags='identifications', fs=fs_tp)
        resp_json = resp.json()

        return fs_tp, resp_json, resp.status_code


class Identity:
    def __init__(self, name, pre_dir, baseline, test, num_baseline):
        self.name = re.sub('\.', '-', name)
        self.pre_dir = pre_dir

        self.baseline = [] + baseline[:num_baseline]

        _test = baseline[num_baseline:] + test

        for i in range(max(num_baseline - len(baseline), 0)):
            a = _test.pop()
            self.baseline.append(a)

        self.test_images = _test

        _b = os.path.basename(self.baseline[0]).split('.')
        self.uid = self.name + '_' + str(_b[0])



class ImageSize:
    def __init__(self, height, width, default_max_dim):
        self.default_max_dim = default_max_dim
        self.height, self.width = height, width

    def __iter__(self):
        yield self.height
        yield self.width

    def __next__(self):
        yield self.height
        yield self.width

    @property
    def aspect_ratio(self):
        return self.width / self.height

    @property
    def scale_ratio(self):
        ratio_x = (numpy.array([self.default_max_dim, self.default_max_dim]) / numpy.array(list(self))).tolist()
        ratio = min(ratio_x)
        return ratio



class Dataset:
    def __init__(self, wkdir, data_dir, cache_dir, num_baseline, num_test, min_len, experiment_set):
        # in case min_len is to big, change it to sum of (num_baseline + num_test):
        _min_l = min(min_len, num_baseline + num_test)
        # in case _min_l is too small, min_length should have at least 1 more image for test
        min_length = max(_min_l, num_baseline + 1)
        self.cache_dir = cache_dir
        if 'cfp-dataset' in data_dir:
            self.identities = self.organize_data_set_cfp(wkdir, data_dir, num_baseline, min_length)
        else:
            self.identities = self.organize_data_set(wkdir, data_dir, num_baseline, min_length)
        print("total # of identities:", len(self.identities))

        self.description = {'num_baseline': num_baseline,
                            'num_test': num_test,
                            'min_len': min_length,
                            'data_dir': data_dir,
                            'num_uids': len(self.identities)}

        self.experiment_set = experiment_set

    @staticmethod
    def organize_data_set(wkdir, xdir, num_baseline, min_len):
        _dataset = {}
        tree = list(os.walk(os.path.join(wkdir, xdir)))
        for t in tree[1:]:
            p_name = os.path.basename(t[0])
            sorted_file_list = sorted(t[2])
            if p_name != 'face' and len(t[2]) >= min_len:
                baseline = list(filter(lambda x: x.endswith('.base.jpg'), sorted_file_list))
                test = list(filter(lambda x: x.endswith('.jpg'), sorted_file_list))
                identity_x = Identity(name=p_name, pre_dir=t[0], baseline=baseline, test=test,
                                      num_baseline=num_baseline)
                _dataset[identity_x.name] = identity_x
            else:
                print('skip', p_name, 'with length', len(t[2]))
        return _dataset


    def organize_data_set_cfp(self, wkdir, xdir, num_baseline, min_len):
        _dataset = {}
        # tree = list(os.walk(os.path.join(wkdir, xdir)))
        people_list = os.listdir(os.path.join(wkdir, xdir))
        for pp in people_list:
            _front = sorted(os.listdir(os.path.join(wkdir, xdir, pp, self.experiment_set)))
            if len(_front) >= min_len:
                front = list(map(lambda x: os.path.join('frontal', x), _front))
                # _profile = os.listdir(os.path.join(wkdir, xdir, pp, 'profile'))
                # profile = list(map(lambda x: os.path.join('frontal', x), _profile))
                baseline = [front.pop(), front.pop()]
                identity_x = Identity(name=pp, pre_dir=os.path.join(wkdir, xdir, pp), baseline=baseline, test=front,
                                      num_baseline=num_baseline)
                _dataset[identity_x.name] = identity_x

        return _dataset

    @staticmethod
    def output_face_w_lower_resolution(im_path, default_max_dim, pp_dir):
        im = cv2.imread(im_path)
        _im_shape = im.shape[:2]
        old_size = ImageSize(height=_im_shape[0], width=_im_shape[1], default_max_dim=default_max_dim)

        _fn = os.path.join(pp_dir, 'lr-'+os.path.basename(im_path))
        print('writing to {}'.format(_fn))
        if old_size.scale_ratio < 1.0:
            _new_height, _new_width = tuple([int(x * old_size.scale_ratio) for x in old_size])
            new_size = ImageSize(height=_new_height, width=_new_width, default_max_dim=default_max_dim)
            im_resized = cv2.resize(im, (new_size.width, new_size.height))
            img_binary = cv2.imencode('.jpg', im_resized)
            cv2.imwrite(filename=_fn, img=im_resized)
        else:
            img_binary = cv2.imencode('.jpg', im)
            cv2.imwrite(filename=_fn, img=im)

        return img_binary

    @staticmethod
    def lower_down_resolution_cfp(wkdir, xdir, save_dir):
        _dataset = {}
        people_list = os.listdir(os.path.join(wkdir, xdir))
        for pp in people_list:
            print("processing", pp)
            _front = sorted(os.listdir(os.path.join(wkdir, xdir, pp, 'frontal')))
            pp_dir = os.path.join(wkdir, save_dir, pp)
            os.makedirs(pp_dir, exist_ok=True)
            for _f in _front:
                Dataset.output_face_w_lower_resolution(im_path=os.path.join(wkdir, xdir, pp, 'frontal', _f),
                                                       default_max_dim=120, pp_dir=pp_dir)
                cc = 0

                # front = list(map(lambda x: os.path.join('frontal', x), _front))
                # # _profile = os.listdir(os.path.join(wkdir, xdir, pp, 'profile'))
                # # profile = list(map(lambda x: os.path.join('frontal', x), _profile))
                # baseline = [front.pop(), front.pop()]
                # identity_x = Identity(name=pp, pre_dir=os.path.join(wkdir, xdir, pp), baseline=baseline, test=front,
                #                       num_baseline=num_baseline)
                # _dataset[identity_x.name] = identity_x

        return _dataset


class BatchOperations:
    def __init__(self,
                 identity_dataset: Dataset,
                 num_in_scope, cache_dir,
                 num_negative_sample_identities,
                 identification_distraction_size,
                 window_size=20,
                 chunk_size=5):
        """

        :param identity_dataset:
        :param num_in_scope: Given the dataset how, many is inscope
        :param cache_dir:
        :param num_negative_sample_identities:
        :param window_size:
        :param chunk_size:
        """
        self.cache_dir = cache_dir
        self.bap = BAPIdentityInterface()
        self.identity_dataset = identity_dataset
        sorted_identity = sorted(list(self.identity_dataset.identities.items()), key=lambda k: k[0])
        self.sorted_identities = sorted_identity
        self.identities_in_scope = sorted_identity[:num_in_scope]
        self.identities_out_scope = sorted_identity[num_in_scope:]
        self.num_in_scope = num_in_scope

        # if not os.path.exists(os.path.join(*cache_dir)):
        os.makedirs(os.path.join(*cache_dir), exist_ok=True)
        self.identification_distraction_size = identification_distraction_size
        self.verification_negative_sample_size = num_negative_sample_identities
        self.identification_test_sample_images_pp = 0
        self.n = window_size
        self.d = chunk_size

    def reset_database(self):
        print('resetting...')
        self.bap.delete_all_identities()
        _, uid_list = self.bap.list_all_identities()
        if not uid_list:
            print('================= reset successfully ==================')

    def batch_create_identity(self):

        for name, identity in self.identities_in_scope:
            assert name == identity.name

            self.bap.create_identity(uid=identity.uid, display_name=identity.name,
                                     img_path_list=identity.baseline,
                                     pre_dir=identity.pre_dir,
                                     metadata=json.dumps(identity.__dict__))
        return

    def unit_verification(self, id_as_claimed, id_tobe_verified, verification_test_sample_images_pp):
        res = []
        meta = {
            'claim_uid': id_as_claimed.uid,
            'claim_name': id_as_claimed.name,
            'target_uid': id_tobe_verified.uid,
            'target_name': id_tobe_verified.name,
            'target_img': '',
            'label': id_as_claimed.uid == id_tobe_verified.uid
        }
        for im in id_tobe_verified.test_images[:verification_test_sample_images_pp]:
            meta.update({'target_img': [id_tobe_verified.pre_dir, im]})
            _, resp, s_code = self.bap.create_verification(uid=id_as_claimed.uid,
                                                           img_path_list=[im],
                                                           pre_dir=id_tobe_verified.pre_dir,
                                                           metadata=json.dumps(meta))
            if s_code == 201:
                res.append(resp)
                # print(resp['items'][0]['score'])
            else:
                print(resp)
                print(id_tobe_verified.pre_dir, im)
        return res

    def unit_verification_bulk(self, id_as_claimed, id_tobe_verified):

        _, resp, s_code = self.bap.create_verification(uid=id_as_claimed.uid,
                                                       img_path_list=id_tobe_verified.test_images,
                                                       pre_dir=id_tobe_verified.pre_dir,
                                                       metadata=json.dumps(id_tobe_verified.__dict__))
        return [resp]

    def batch_create_verification(self, verification_test_sample_images_pp, bk=False):

        resp_list = []
        for i, item in enumerate(self.identities_in_scope):

            name, identity = item
            print('\n---------', 'verifying', name, '----------{}/{}'.format(i, len(self.identities_in_scope)))
            assert name == identity.name
            population = list(filter(lambda x: x != i, range(len(self.identities_in_scope))))
            distraction = list(filter(lambda x: x != i, range(len(self.identities_in_scope),
                                                              len(self.sorted_identities))))

            assert i not in population
            neg_in_scope = random.sample(population, min(len(population), self.verification_negative_sample_size))
            if distraction:
                neg_out_scope = random.sample(distraction,
                                              min(len(distraction), self.verification_negative_sample_size))
            else:
                neg_out_scope = []

            sample_pool = neg_in_scope + neg_out_scope + [i]

            for ix in sample_pool:
                name2, identity2 = self.sorted_identities[ix]
                print('negative sample:' if i != ix else 'positive sample', name2)
                # for name2, identity2 in self.sorted_identities:
                if not bk:
                    resp_ = self.unit_verification(
                        id_as_claimed=identity, id_tobe_verified=identity2,
                        verification_test_sample_images_pp=verification_test_sample_images_pp)
                else:
                    resp_ = self.unit_verification_bulk(id_as_claimed=identity, id_tobe_verified=identity2)

                resp_list.append(resp_)

        self.analyse_verification_results(resp_list)

        return resp_list

    def _prc_draw(self, y_true, y_pred, title):
        precision, recall, thresholds = sklearn.metrics.precision_recall_curve(y_true, y_pred)
        average_precision = average_precision_score(y_true=y_true, y_score=y_pred)

        plt.figure()
        plt.step(recall, precision, color='b', alpha=0.2,
                 where='post')
        plt.fill_between(recall, precision, alpha=0.2, color='b', **{'step': 'post'})

        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.ylim([0.0, 1.05])
        plt.xlim([0.0, 1.0])
        plt.title('{0} Precision-Recall curve: AP={1:0.2f}'.format(title,
                                                                   average_precision))

        prc_path = os.path.join(*self.cache_dir, '{}_prc'.format(title))

        plt.savefig(prc_path, dpi=None, facecolor='w', edgecolor='w',
                    orientation='portrait', papertype=None, format=None,
                    transparent=False, bbox_inches=None, pad_inches=0.1,
                    frameon=None, metadata=None)

        return precision, recall, thresholds

    def _draw_threshold_vs_f_score(self, precision, recall, thresholds, title):
        f_scores = 2 * precision * recall / (precision + recall)
        plt.figure()
        plt.plot(thresholds, f_scores[:-1], color='r')
        plt.fill_between(thresholds, f_scores[:-1], alpha=0.2, color='r')

        plt.xlabel('Threshold')
        plt.ylabel('F1-score')
        plt.ylim([0.0, 1.05])
        plt.xlim([0.0, 1.0])
        plt.title('{} F1 vs threshold'.format(title))

        prc_path = os.path.join(*self.cache_dir, '{}_threshold_vs_f_score'.format(title))

        plt.savefig(prc_path, dpi=None, facecolor='w', edgecolor='w',
                    orientation='portrait', papertype=None, format=None,
                    transparent=False, bbox_inches=None, pad_inches=0.1,
                    frameon=None, metadata=None)

        return f_scores

    def _r1_prc(self, y_true, y_pred, title):
        text = ''

        precision, recall, thresholds = self._prc_draw(y_true, y_pred, title)

        f_scores = self._draw_threshold_vs_f_score(precision, recall, thresholds, title)

        arg_best = numpy.argmax(f_scores)


        text += "==== f-scores: ====\n" + self._render_best_n(f_scores, arg_best)
        text += "==== thresholds: ====\n" + self._render_best_n(thresholds, arg_best)

        text += "---- BEST f-score/threshold: {}/{}, achieved @ {}\n".format(f_scores[arg_best],
                                                                             thresholds[arg_best], arg_best)

        text += "\n-----------------------------------------\n.\n\n"
        return text

    def _draw_threshold_vs_b_accuracy(self, fpr, tpr, thresholds, title):

        tnr = 1 - fpr
        fnr = 1 - tpr
        balanced_accuracy = (tpr + tnr) / 2

        plt.figure()
        plt.plot(thresholds, balanced_accuracy, color='r')
        plt.fill_between(thresholds, balanced_accuracy, alpha=0.2, color='r')

        plt.xlabel('Threshold')
        plt.ylabel('B.accuracy')
        plt.ylim([0.0, 1.05])
        plt.xlim([0.0, 1.0])
        plt.title('{} B.accuracy vs threshold'.format(title))

        prc_path = os.path.join(*self.cache_dir, '{}_threshold_vs_b_accuracy'.format(title))

        plt.savefig(prc_path, dpi=None, facecolor='w', edgecolor='w',
                    orientation='portrait', papertype=None, format=None,
                    transparent=False, bbox_inches=None, pad_inches=0.1,
                    frameon=None, metadata=None)

        return balanced_accuracy, tpr, tnr

    def _roc_draw(self, y_true, y_pred, title):
        fpr, tpr, thresholds = sklearn.metrics.roc_curve(y_true=y_true, y_score=y_pred, pos_label=1,
                                                         drop_intermediate=False)
        auc_score = roc_auc_score(y_true=y_true, y_score=y_pred)

        plt.figure()
        lw = 2
        plt.plot(fpr, tpr, color='darkorange', label='ROC curve for {}'.format(title))
        plt.fill_between(fpr, tpr, alpha=0.2, color='darkorange')
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('{} ROC, auc_score: {}'.format(title, auc_score))
        plt.legend(loc="lower right")

        roc_path = os.path.join(*self.cache_dir, '{}_roc'.format(title))

        plt.savefig(roc_path, dpi=None, facecolor='w', edgecolor='w',
                    orientation='portrait', papertype=None, format=None,
                    transparent=False, bbox_inches=None, pad_inches=0.1,
                    frameon=None, metadata=None)
        return fpr, tpr, thresholds

    def _render_best_n(self, arr, arg_best):
        text = ''
        b_position = min(arg_best, self.n)
        window = arr[max(0, arg_best - self.n): arg_best + self.n]
        window_str = list(
            map(lambda iw: '{0:0.4f}*'.format(iw[1]) if iw[0] == b_position else '{0:0.4f}'.format(iw[1]),
                enumerate(window)))
        for i in range(0, len(window_str), self.d):
            text += '\t\t'.join(window_str[i: i + self.d]) + '\n'

        text += '.\n'
        return text

    def _r2_roc(self, y_true, y_pred, title='verification'):
        text = ''

        fpr, tpr, thresholds = self._roc_draw(y_true, y_pred, title)
        balanced_accuracy, tpr, tnr = self._draw_threshold_vs_b_accuracy(fpr, tpr, thresholds, title)

        arg_best = numpy.argmax(balanced_accuracy)

        text += "==== fpr: ====\n" + self._render_best_n(fpr, arg_best)
        text += "==== tpr: ====\n" + self._render_best_n(tpr, arg_best)
        text += "==== tnr: ====\n" + self._render_best_n(tnr, arg_best)
        text += "==== thresholds: ====\n" + self._render_best_n(thresholds, arg_best)
        text += "==== B.accuracy: ====\n" + self._render_best_n(balanced_accuracy, arg_best)

        text += "---- BEST B.accuracy/threshold: {}/{} ----\n---- tpr/tnr: {}/{}, achieved @ {}  ----\n".format(
            balanced_accuracy[arg_best], thresholds[arg_best], tpr[arg_best], tnr[arg_best], arg_best)

        return text

    def analyse_verification_results(self, resp_list):
        response_list = list(itertools.chain(*resp_list))
        truth_pred_pair = list(map(lambda x: (json.loads(x['data']), x['items']), response_list))
        ground_truth, prediction = list(zip(*truth_pred_pair))

        y_true = numpy.array(list(map(lambda x: int(x['label']), ground_truth)))
        y_pred = numpy.array(list(map(lambda x: x[0]['score'], prediction)))

        text = ''
        text += '# positive test: {}\n'.format(sum(y_true))
        text += '# negative test: {}\n'.format(len(y_true) - sum(y_true))

        text_sec = self._r1_prc(y_true, y_pred, title='verification')
        text += text_sec

        text_sec = self._r2_roc(y_true, y_pred, title='verification')
        text += text_sec

        with open(os.path.join(*self.cache_dir, 'verification.txt'), mode='w', encoding='utf8') as f_:
            f_.write(text)

        # try:
        #     with open(os.path.join(*self.cache_dir, 'verification_truth'), mode='wb') as f_:
        #         pickle.dump(y_true, file=f_)
        #
        #     with open(os.path.join(*self.cache_dir, 'verification_pred'), mode='wb') as f_:
        #         pickle.dump(y_pred, file=f_)
        # except Exception:
        #     pass

    def create_or_update_global_identity_sets(self):
        self.bap.create_or_update_global_identity_set()
        return

    def unit_identification(self, test_set, identification_test_sample_images_pp):
        resp_list = []
        for name, identity in test_set:
            print('testing identification with name {}.'.format(name))

            for im in identity.test_images[:identification_test_sample_images_pp]:

                meta = {'target_img': [identity.pre_dir, im],
                        'ground_truth': identity.uid,
                        'ground_truth_name': identity.name}

                _, resp, s_code = self.bap.create_identification(uid_set=self.bap.global_uid_set,
                                                                 img_path_list=[im],
                                                                 threshold=0.5,
                                                                 top_n=10,
                                                                 match_only=False,
                                                                 pre_dir=identity.pre_dir,
                                                                 metadata=json.dumps(meta))

                if s_code == 201:
                    resp_list.append(resp)
                    # print('received:', resp['count'])
                else:
                    print(resp)
                    print(identity.pre_dir, im)
        return resp_list

    def batch_create_identification(self, identification_test_sample_images_pp=2):
        print('\n\n=============================\n\n')
        self.identification_test_sample_images_pp = identification_test_sample_images_pp
        """
        {
            "identity_set_name": "0-global-set",
            "threshold": 0.3,
            "top_n": 5,
            "match_only": False,
            "data": ""
        }

        :param num_sample_pp: num of sample per person
        :return:

        """
        test_set_a = self.identities_in_scope
        print('-------- test in_scope -------')
        resp_list_in_scope = self.unit_identification(test_set_a, identification_test_sample_images_pp)

        test_set_b = random.sample(self.identities_out_scope,
                                   min(len(self.identities_out_scope), self.identification_distraction_size))
        print('-------- test distraction -------')
        resp_list_distraction = self.unit_identification(test_set_b, identification_test_sample_images_pp)

        self.analyse_identification_results(resp_list_in_scope, resp_list_distraction)

        return resp_list_in_scope, resp_list_distraction

    @staticmethod
    def _mat_ex(list_x):
        def _ex(item_x):
            return list(map(lambda x: (x['identity']['name'], x['items'][0]['score']), item_x))

        truth_pred_pair = list(map(lambda x: (json.loads(x['data']), x['items']), list_x))
        ground_truth, prediction = list(zip(*truth_pred_pair))
        _y_pred = list(map(lambda xl: _ex(xl), prediction))
        # y_pred_mat.shape: top_n, n_identity, 2
        y_pred_mat = numpy.transpose(numpy.array(_y_pred), axes=[1, 0, 2])
        truth_label = numpy.array(list(map(lambda x: x['ground_truth'], ground_truth)))
        pred_label = numpy.squeeze(y_pred_mat[:, :, 0])
        pred_score = numpy.squeeze(y_pred_mat[:, :, 1])
        y_true = (pred_label == truth_label).astype(dtype=numpy.int)
        return y_true, pred_score, pred_label

    def analyse_identification_results(self, resp_list_in_scope, resp_list_distraction):

        y_true_in_scope, pred_score_in_scope, _ = self._mat_ex(resp_list_in_scope)

        retrieval_distribution = numpy.sum(y_true_in_scope, axis=-1) / y_true_in_scope.shape[1]

        plt.figure()

        plt.ylim([0.0, 1.05])
        plt.xlabel('Percentage')
        plt.ylabel('Rank Top_n')
        plt.title('distribution of correctly retrieved identity over the retrieval ranks (1-10)')
        plt.bar(range(len(retrieval_distribution)), retrieval_distribution)
        # plt.xticks(x, ('Bill', 'Fred', 'Mary', 'Sue'))
        barc_path = os.path.join(*self.cache_dir, '{}_retrieval_distribution'.format('identification'))
        plt.savefig(barc_path, dpi=None, facecolor='w', edgecolor='w',
                    orientation='portrait', papertype=None, format=None,
                    transparent=False, bbox_inches=None, pad_inches=0.1,
                    frameon=None, metadata=None)

        y_true_distraction, pred_score_distraction, _ = self._mat_ex(resp_list_distraction)

        y_true = numpy.concatenate([y_true_in_scope, y_true_distraction], axis=1).ravel()

        y_pred = numpy.concatenate([pred_score_in_scope, pred_score_distraction],
                                   axis=1).ravel().astype(dtype=numpy.float)

        text = ''
        text += '# in_scope: {}\n'.format(y_true_in_scope.shape[1])
        text += '# distraction: {}\n'.format(y_true_distraction.shape[1])

        text += "--- distribution of correctly retrieved identity over the retrieval ranks (1-10) ---\n"
        accumulate_acc = 0
        for ei, frac in enumerate(retrieval_distribution):
            accumulate_acc += frac
            text += 'rank {}: {}  ***  top_{} acc: {}\n'.format(ei + 1, frac, ei + 1, accumulate_acc)

        text += '---------------------------\n.\n\n'

        text_sec = self._r1_prc(y_true, y_pred, title='identification')
        text += text_sec
        # mm = 0
        text_sec = self._r2_roc(y_true, y_pred, title='identification')
        text += text_sec

        with open(os.path.join(*self.cache_dir, 'identification.txt'), mode='w', encoding='utf8') as f_:
            f_.write(text)

        # try:
        #     with open(os.path.join(*self.cache_dir, 'identification_truth'), mode='wb') as f_:
        #         pickle.dump(y_true, file=f_)
        #
        #     with open(os.path.join(*self.cache_dir, 'identification_pred'), mode='wb') as f_:
        #         pickle.dump(y_pred, file=f_)
        # except Exception:
        #     pass

        return

    def summary_config(self):
        d = dict()
        d["test_size"] = self.num_in_scope
        d["verification_negative_sample_size"] = self.verification_negative_sample_size
        d["identification_distraction_size"] = self.identification_distraction_size
        d["identification_test_sample_images_pp"] = self.identification_test_sample_images_pp
        d["data_set"] = {'num_baseline': self.identity_dataset.description}

        with open(os.path.join(*self.cache_dir, 'config.txt'), mode='w', encoding='utf8') as f_:
            json.dump(d, fp=f_, indent=4)

        print(json.dumps(d, indent=4))
        print('---- DONE ----')


class DataSetLookUp:
    def __init__(self):
        self.repo = {'cfp': 'data/cfp-dataset/Data/Images/',
                     'cfp-lower-res': 'data/cfp-lower-res/Data/Images/',
                     'facescrub': 'facescrub/download5'}

    def get_data_dir_by_name(self, name):
        return self.repo.get(name)


def exp_run(data_set='cfp',
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=4,
            test_size=200,
            verification_negative_identity_sample=50,
            identification_distraction_size=50,
            reset=False,
            mode='vd',
            title='NA',
            experiment_set='frontal'):
    """

    :param title: customise your experiment folder (it will contain the experiment results)
    :param experiment_set: use 'frontal' as default value.
    :param data_set: choose from 'cfp-lower-res', 'cfp'
    :param num_baseline_per_id: how many image to enroll per identity
    :param num_test_per_id: how many image per identity are used for testing
    :param min_sample_per_id: normally should be （num_test_per_id+num_baseline_per_id） to ensure consistency
    :param test_size: total num of people to be enrolled into the system
    :param verification_negative_identity_sample: num of distraction identity (not enrolled into the system)
    :param identification_distraction_size: num of distraction identity (not enrolled into the system)
    :param reset: reset database. default should be true
    :param mode: 'v', 'd', 'vd' for verification only, identification only, both
    :return:
    """
    experiment_cache = 'experiment/experiment-{}-{}'.format(title,
                                                            datetime.now().strftime('%Y-%m-%d %H.%M.%S.%f')[:-5])


    data_dir = DataSetLookUp().get_data_dir_by_name(name=data_set)

    wkdir = os.path.dirname(os.path.abspath(''))
    dataset = Dataset(wkdir=wkdir, data_dir=data_dir, cache_dir=experiment_cache,
                      num_baseline=num_baseline_per_id, num_test=num_test_per_id, min_len=min_sample_per_id,
                      experiment_set=experiment_set)

    batch_operations = BatchOperations(identity_dataset=dataset,
                                       num_in_scope=test_size,
                                       cache_dir=[wkdir, experiment_cache],
                                       num_negative_sample_identities=verification_negative_identity_sample,
                                       identification_distraction_size=identification_distraction_size)
    if reset:
        if not RESET:
            a = input('Are going to erase the data base? Are you fucking sure?? (yes/no)')
        else:
            a = 'yes'
        if a.lower() == 'yes':
            batch_operations.reset_database()
            batch_operations.bap.delete_global_identity_set()
        else:
            print('Abort!')
    batch_operations.batch_create_identity()

    batch_operations.create_or_update_global_identity_sets()

    if 'v' in mode:
        batch_operations.batch_create_verification(verification_test_sample_images_pp=num_test_per_id)
    if 'd' in mode:
        batch_operations.batch_create_identification(identification_test_sample_images_pp=num_test_per_id)

    batch_operations.summary_config()




RESET = True
if __name__ == '__main__':

    # Global setting confirmation:
    EXPERIMENT_SUB_SET = 'frontal'
    DATA_SET = 'cfp-lower-res'
    EXPERIMENT_MODE = 'd'

    if DATA_SET == 'cfp-lower-res':
        a = input('You want to **lower** the resolution, correct? (yes/no)')
        _ds_ = DataSetLookUp()
        if a.lower() != 'yes':
            exit("Please check you configuration")
        else:
            data_dir = 'data/cfp-lower-res/Data/Images/'
            Dataset.lower_down_resolution_cfp(wkdir=os.path.dirname(os.path.abspath('')),
                                              xdir=_ds_.get_data_dir_by_name('cfp'),
                                              save_dir=_ds_.get_data_dir_by_name(DATA_SET))

    if RESET:
        a = input("\n**RESET** database will performed on every experiment, are you sure? (yes/no)")
        if a.lower() != 'yes':
            exit("Please check you configuration")

    if EXPERIMENT_MODE:
        a = input('\nThe experiment mode is set as **{}** '
                  'for all experiments. Are you sure? (yes/no)'.format(EXPERIMENT_MODE))
        if a.lower() != 'yes':
            exit("Please Change you configuration")

    # test_pipeline
    exp_run(data_set=DATA_SET,
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=3,
            verification_negative_identity_sample=5,
            identification_distraction_size=3, reset=True,
            mode=EXPERIMENT_MODE,
            title='pipeline',
            experiment_set=EXPERIMENT_SUB_SET)
    # exit()
    # large b2

    exp_run(data_set=DATA_SET,
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=400,
            verification_negative_identity_sample=100,
            identification_distraction_size=100, reset=True,
            mode=EXPERIMENT_MODE,
            title='large',
            experiment_set=EXPERIMENT_SUB_SET)

    # large b1
    exp_run(data_set=DATA_SET,
            num_baseline_per_id=1,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=400,
            verification_negative_identity_sample=100,
            identification_distraction_size=100, reset=True,
            mode=EXPERIMENT_MODE,
            title='large',
            experiment_set=EXPERIMENT_SUB_SET)

    # mid b2
    exp_run(data_set=DATA_SET,
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=200,
            verification_negative_identity_sample=100,
            identification_distraction_size=100, reset=True,
            mode=EXPERIMENT_MODE,
            title='mid',
            experiment_set=EXPERIMENT_SUB_SET)

    # mid b1
    exp_run(data_set=DATA_SET,
            num_baseline_per_id=1,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=200,
            verification_negative_identity_sample=100,
            identification_distraction_size=100, reset=True,
            mode=EXPERIMENT_MODE,
            title='mid',
            experiment_set=EXPERIMENT_SUB_SET)

    # mid-small
    exp_run(data_set=DATA_SET,
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=100,
            verification_negative_identity_sample=50,
            identification_distraction_size=200, reset=True,
            mode=EXPERIMENT_MODE,
            title='midsm',
            experiment_set=EXPERIMENT_SUB_SET)

    # small
    exp_run(data_set=DATA_SET,
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=50,
            verification_negative_identity_sample=20,
            identification_distraction_size=100, reset=True,
            mode=EXPERIMENT_MODE,
            title='small',
            experiment_set=EXPERIMENT_SUB_SET)

    # tiny
    exp_run(data_set=DATA_SET,
            num_baseline_per_id=2,
            num_test_per_id=5,
            min_sample_per_id=7,
            test_size=20,
            verification_negative_identity_sample=10,
            identification_distraction_size=100, reset=True,
            mode=EXPERIMENT_MODE,
            title='tiny',
            experiment_set=EXPERIMENT_SUB_SET)

