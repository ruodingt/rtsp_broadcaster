import os
import re
from datetime import datetime

import cv2
import numpy as np

from project_types import ImageSize


def adjust_gamma(image, gamma=1.0):
    if gamma == 1.0:
        return image
    inv_gamma = 1.0 / gamma
    table = np.array([
        ((i / 255.0) ** inv_gamma) * 255
        for i in np.arange(0, 256)])
    return cv2.LUT(image.astype(np.uint8), table.astype(np.uint8))



def original_frame_generator(fp, gamma, c_rate=0.1, size=ImageSize(height=1080, width=1920), lb=0, tb=0):
    # Create a VideoCapture object and read from input file
    # If the input is the camera, pass 0 instead of the video file name
    cap = cv2.VideoCapture(fp)
    # Check if camera opened successfully
    if cap.isOpened() is False:
        print("Error opening video stream or file")
    # Read until video is completed
    while cap.isOpened():
        # Capture frame-by-frame
        ret, _frame = cap.read()
        if ret is True:
            frame = adjust_gamma(_frame, gamma=gamma)
            _o_size = frame.shape[:2]
            o_size = ImageSize(height=_o_size[0], width=_o_size[1])
            if c_rate >0:
                _t, _l = int(o_size.height*c_rate), int(o_size.width*c_rate)
                xed_frame = frame[_t:(o_size.height - _t), (_l+lb):(o_size.width - _l +lb)]
            elif c_rate < 0:
                _t, _l = int(-o_size.height * c_rate), int(-o_size.width * c_rate)
                xed_frame = cv2.copyMakeBorder(frame, _t, _t, left=_l+lb, right=_l-lb,
                                               borderType=cv2.BORDER_CONSTANT, value=[0, 0, 0])
            else:
                xed_frame = frame

            fframe = cv2.resize(xed_frame, (size.width, size.height))

            # Display the resulting frame
            yield fframe
        # Break the loop
        else:
            break
    cap.release()



def _write_frame_video(video_fp,
                       codec,
                       frame_rate,
                       size,
                       frame_generator,
                       verbose=1000,
                       start=900,
                       end=1000):

    out = cv2.VideoWriter(video_fp, cv2.VideoWriter_fourcc(*codec), frame_rate,
                          (size.width, size.height))
    fi = 0
    if start:
        first_frame = next(frame_generator)

        for i in range(start*frame_rate):
            if fi % verbose == 1:
                print(fi, 'frames generated')
            fi += 1
            out.write(first_frame)

    for frame in frame_generator:

        # img_rs = cv2.resize(frame, (size.width, size.height))
        # img_rs = cv2.resize(cv2.imread(filename), (DEFAULT_SIZE.width, DEFAULT_SIZE.height))
        assert tuple(frame.shape[:2]) == tuple(size)
        if fi % verbose == 1:
            print(fi, 'frames generated')
        fi += 1
        out.write(frame)


    if end:
        last_frame = frame
        # img_rs = cv2.resize(last_frame, (size.width, size.height))
        for i in range(end*frame_rate):
            if fi % verbose == 1:
                print(fi, 'frames generated')
            fi += 1
            out.write(last_frame)

    out.release()

    print('Export video at {}'.format(video_fp))

    print('Done with ', codec)


def write_video(wkdir, codec, form, frame_generator, size: ImageSize, verbose=1000,
                frame_rate=30, tag='res', start=900, end=1000):


    # datetime object containing current date and time
    now = str(datetime.now())
    now = re.sub(':', '-', now)

    dir_head = os.path.join(wkdir, 'output_edit')
    if not os.path.exists(dir_head):
        os.makedirs(dir_head)

    video_fp = os.path.join(dir_head, '{}_{}_{}.{}'.format(tag, now, codec, form))

    _write_frame_video(video_fp,
                       codec,
                       frame_rate,
                       size,
                       frame_generator,
                       verbose=verbose,
                       start=start,
                       end=end)




if __name__ == "__main__":
    wkdir = os.path.dirname(os.path.dirname(__file__))
    bench_video_fp = os.path.join(wkdir, 'data', 'v9.mp4')
    frame_size = ImageSize(height=1080, width=1920)
    frame_generator_m = original_frame_generator(fp=bench_video_fp, gamma=1.0, size=frame_size, c_rate=-0.4, lb=0)
    write_video(wkdir,
                codec='XVID',
                form='mp4',
                frame_generator=frame_generator_m,
                size=frame_size, #720/1280
                verbose=1000,
                frame_rate=30, tag='res', start=900, end=1000)


