import glob
import heapq
import json
import os
from collections import Counter, defaultdict, OrderedDict
from functools import reduce
from typing import Optional
import pickle
import cv2
from tqdm import tqdm

from config import ALL_PIC_SETS, DEFAULT_HEIGHT, DEFAULT_WIDTH, IDEAL_FACE_HEIGHT
from project_types import ImageSize, AgeGenderGroup
from face_detector_x import ProjectFaceDetector

DEFAULT_SIZE = ImageSize(height=DEFAULT_HEIGHT, width=DEFAULT_WIDTH)


# def meta_summary(filename_list):
#     meta_info_set = list(map(lambda s: os.path.basename(s).split('_')[:2], filename_list))
#     age, gender = list(zip(*meta_info_set))
#     age_distribution = Counter(age)
#     gender_distribution = Counter(gender)
#     age_dist = Counter()
#     for _age_m, _num_m in age_distribution.items():
#         age_scope = int(int(_age_m) / 10)
#         age_dist["age_scope_" + str(age_scope)] += int(_num_m)
#     total = len(age)
#     return age_distribution, gender_distribution, total


class PictureFolderTree:
    def __init__(self):
        self.wkdir = os.path.dirname(os.path.dirname(__file__))
        self.root_dir = os.path.join(self.wkdir, 'demographics')
        self.sub_folders = OrderedDict()

    def collect_files_from(self, sub_dir='part1'):
        _files_list = list(glob.glob(os.path.join(self.wkdir, self.root_dir, sub_dir, '*.jpg')))
        self.sub_folders[sub_dir] = _files_list
        return _files_list

    def load_all_folders(self):
        for ss in ALL_PIC_SETS:
            self.collect_files_from(sub_dir=ss)
        return self.sub_folders


class PicFrame:
    def __init__(self, file_path, face_detector: ProjectFaceDetector, wkdir,
                 age_gender_group: AgeGenderGroup, enable_cache=True):
        self.file_path = file_path
        self.face_detector = face_detector

        self.group = age_gender_group  # : Optional[AgeGenderGroup]

        self.size = None # : Optional[ImageSize]
        self.compatibility_loss = None
        self.scale_ratio = None
        self.reference_b_box_pre_scaled = None
        self.tf_pre_scale_ratio = None
        self.reference_score = None

        self._populate_meta_info(wkdir, enable_cache)

    def is_valid(self):
        return self.size is not None \
               and self.compatibility_loss is not None \
               and self.scale_ratio is not None \
               and self.tf_pre_scale_ratio is not None \
               and self.reference_b_box_pre_scaled is not None

    def _get_compatibility_loss_from_img_size(self, lamdbax=1.0):
        # loss should be as small as possible
        return abs(self.size.aspect_ratio - DEFAULT_SIZE.aspect_ratio) + lamdbax * self.size.scale_ratio

    def _get_compatibility_loss_from_face_size(self, score):
        # loss should be as small as possible
        if self.reference_b_box_pre_scaled:
            self.scale_ratio = self.reference_b_box_pre_scaled.scale_ratio
            return self.scale_ratio * self.tf_pre_scale_ratio # abs(self.size.aspect_ratio - DEFAULT_SIZE.aspect_ratio) +
        else:
            return 100000.0

    def pre_scale(self, im):
        original_size = im.shape[:2]
        _new_height, _new_width = tuple([int(x * self.tf_pre_scale_ratio) for x in original_size])
        new_size = ImageSize(height=_new_height, width=_new_width)
        resized_img = cv2.resize(im, (new_size.width, new_size.height))
        return resized_img

    def _populate_meta_info(self, wkdir, enable_cache):
        # TODO: Just want to capture the meta information, should be a better way
        fpp, fbase = os.path.split(self.file_path+'.pickle')
        _, fpb = os.path.split(fpp)
        meta_path = os.path.join(wkdir, 'output_'+str(IDEAL_FACE_HEIGHT), fpb, fbase)
        meta_path_dir = os.path.dirname(meta_path)

        if not os.path.exists(meta_path_dir):
            os.makedirs(meta_path_dir)

        elif not enable_cache or not os.path.exists(meta_path):
            _im = cv2.imread(self.file_path)
            _im_shape = _im.shape[:2]
            self.size = ImageSize(height=_im_shape[0], width=_im_shape[1])
            self.tf_pre_scale_ratio = self.size.tf_input_scale_ratio_hinge

            # Need to perform pre-scale here because face detector cannot identity small faces
            _im_pre_scaled = self.pre_scale(im=_im)

            _face_b_boxes_pre_scaled, _scores = self.face_detector.get_b_box(_im_pre_scaled)
            if len(_face_b_boxes_pre_scaled) == 1:
                self.reference_b_box_pre_scaled = _face_b_boxes_pre_scaled[0]
                self.reference_score = _scores[0]
            self.compatibility_loss = self._get_compatibility_loss_from_face_size(score=self.reference_score)

            # self.group = AgeGenderGroup(fp=self.file_path)

            self.dump_to_pk(fp=meta_path)

        elif enable_cache and os.path.exists(meta_path):
            self.load_from_pk(fp=meta_path)
            # self.group = AgeGenderGroup(fp=self.file_path)

    def dump_to_pk(self, fp):
        d = dict()
        d_include = ['size', 'scale_ratio', 'reference_b_box_pre_scaled', 'compatibility_loss',
                     'tf_pre_scale_ratio', 'reference_score']
        for k, v in self.__dict__.items():
            if k in d_include:
                d[k] = v
        with open(fp, 'wb') as file:
            pickle.dump(d, file)

    def load_from_pk(self, fp):
        with open(fp, 'rb') as file:
            d = pickle.load(file)
            self.__dict__.update(d)


    def __lt__(self, other):
        return self.compatibility_loss < other.compatibility_loss

    def __gt__(self, other):
        return self.compatibility_loss > other.compatibility_loss


class HeapQx:
    def __init__(self):
        self.h = []
        return

    def heap_push(self, itm):
        heapq.heappush(self.h, itm)

    def get_n_smallest(self, n):
        return heapq.nsmallest(n, self.h, )


class PicRepo:
    def __init__(self, pic_folder_tree: PictureFolderTree, pipeline_test_size=0, group_filter: callable=lambda x: True):
        self.pic_folder_tree = pic_folder_tree
        self.age_gender_group = defaultdict(HeapQx)
        self.detector = ProjectFaceDetector()
        self.group_filter = group_filter

        self._all_pic_paths = self.get_all_pic_path()
        if pipeline_test_size > 0:
            self._all_pic_paths = self._all_pic_paths[:pipeline_test_size]

        self.populate_this_repo()

    def get_all_pic_path(self):
        _pic_folder_tree = self.pic_folder_tree
        _sub_folders = _pic_folder_tree.load_all_folders()
        _all_pic_path = reduce(lambda a, b: a + b, _sub_folders.values())
        return _all_pic_path

    def populate_this_repo(self):
        # progress_total = len(self._all_pic_paths)
        print("Reading pic files...Patience is a virtue.")
        for i in tqdm(range(len(self._all_pic_paths))):
            # Exclude corrupted file:
            # if i not in [4004, 7466, 9682]:
            fp = self._all_pic_paths[i]

            age_gender_group = AgeGenderGroup(fp=fp)

            if self.group_filter(age_gender_group):
                pf = PicFrame(file_path=fp, face_detector=self.detector, wkdir=self.pic_folder_tree.wkdir, age_gender_group=age_gender_group)
                if pf.is_valid():
                    self.age_gender_group[(pf.group.gen_group, pf.group.age_group)].heap_push(itm=pf)
                else:
                    print('Cannot process', pf.file_path)

    def get_best_n_samples_from_each_group(self, n) -> {str: [PicFrame]}:
        selected = dict() # type {str: {PicFrame}}
        for group_name, group in self.age_gender_group.items():
            selected[group_name] = group.get_n_smallest(n)
        # extract top 100 best cases from each age gender group.
        return selected


if __name__ == '__main__':
    pic_repo = PicRepo(pic_folder_tree=PictureFolderTree(), pipeline_test_size=200)
    selected_repo = pic_repo.get_best_n_samples_from_each_group(n=5)
    print('DONE, DONE')
