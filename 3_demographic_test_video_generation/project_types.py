import os

import numpy as np


################################ TYPES: DO NOT TOUCH ##################################
from config import DEFAULT_HEIGHT, DEFAULT_WIDTH, MAX_SCALE, IDEAL_FACE_HEIGHT, \
    TF_INPUT_HEIGHT_MAX, TF_INPUT_WIDTH_MAX, \
    MAX_TF_PRE_SCALE


class ImageSize:
    def __init__(self, height, width):
        self.height, self.width = height, width

    def __iter__(self):
        yield self.height
        yield self.width

    def __next__(self):
        yield self.height
        yield self.width

    @property
    def aspect_ratio(self):
        return self.width / self.height

    @property
    def scale_ratio(self):
        ratio_x = (np.array([DEFAULT_HEIGHT, DEFAULT_WIDTH]) / np.array(list(self))).tolist()
        ratio = min(ratio_x + [MAX_SCALE])
        return ratio

    @property
    def tf_input_scale_ratio_hinge(self):
        ratio_x = (np.array([TF_INPUT_HEIGHT_MAX, TF_INPUT_WIDTH_MAX]) / np.array(list(self))).tolist()
        ratio = min(ratio_x + [MAX_TF_PRE_SCALE])
        return ratio

class BBox:
    def __init__(self, b):
        self.y_min, self.x_min, self.y_max, self.x_max = b

    def __iter__(self):
        yield self.y_min
        yield self.x_min
        yield self.y_max
        yield self.x_max

    def __next__(self):
        yield self.y_min
        yield self.x_min
        yield self.y_max
        yield self.x_max

    @property
    def aspect_ratio(self):
        return (self.x_max - self.x_min) / (self.y_max - self.y_min)

    @property
    def scale_ratio(self):
        ratio = DEFAULT_HEIGHT * IDEAL_FACE_HEIGHT / (self.y_max - self.y_min)
        return ratio


class AgeGenderGroup:
    YoungChildren = 'YoungChildren'
    GAP1 = 'GAP1'
    Teenager = 'Teenager'
    GAP2 = 'GAP2'
    YoungAdult = 'YoungAdult'
    GAP3 = 'GAP3'
    Adult = 'Adult'
    GAP4 = 'GAP4'
    Senior = 'Senior'
    Unknown = 'Unknown'

    def __init__(self, fp):
        self.age, self.gender = self.get_raw_age_gender(fp)
        self.age_group = self.get_age_group(self.age)
        self.gen_group = 'male' if self.gender == '0' else 'female'
        return

    def is_group(self, group_name):
        return self.age_group == group_name

    @staticmethod
    def get_raw_age_gender(fp):
        _age, _gender = os.path.basename(fp).split('_')[:2]
        return _age, _gender

    @classmethod
    def get_age_group_from_fp(cls, fp):
        _age, _ = cls.get_raw_age_gender(fp)
        age_group = cls.get_age_group(_age)
        return age_group

    @staticmethod
    def get_age_group(age_str):
        if int(age_str) <=12:
            return 'YoungChildren'
        elif 13 <= int(age_str) <= 14:
            return 'GAP1'
        elif 15 <= int(age_str) <= 20:
            return 'Teenager'
        elif 21 <= int(age_str) <= 24:
            return 'GAP2'
        elif 25 <= int(age_str) <= 32:
            return 'YoungAdult'
        elif 33 <= int(age_str) <= 37:
            return 'GAP3'
        elif 38 <= int(age_str) <= 53:
            return 'Adult'
        elif 54 <= int(age_str) <= 59:
            return 'GAP4'
        elif 60 <= int(age_str) :
            return 'Senior'
        else:
            return 'Unknown'

    def __iter__(self):
        # emulate the behaviour of namedtuple
        return self.age, self.age_group, self.gender, self.gen_group