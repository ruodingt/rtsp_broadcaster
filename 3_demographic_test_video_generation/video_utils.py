import json
import os
import re
from collections import Counter
from datetime import datetime

import cv2

from config import DEFAULT_HEIGHT, DEFAULT_WIDTH, IDEAL_FACE_HEIGHT
from project_types import ImageSize
from videofy2 import PicFrame
import numpy as np
import copy

DEFAULT_SIZE = ImageSize(height=DEFAULT_HEIGHT, width=DEFAULT_WIDTH)


def zero_padding(img_path, desired_size=DEFAULT_SIZE):
    im = cv2.imread(img_path)
    _im_shape = im.shape[:2]
    old_size = ImageSize(height=_im_shape[0], width=_im_shape[1])  # old_size is in (height, weight) format

    _new_height, _new_width = tuple([int(x * old_size.scale_ratio) for x in old_size])
    new_size = ImageSize(height=_new_height, width=_new_width)

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size.width, new_size.height))

    delta_h = desired_size.height - new_size.height
    delta_w = desired_size.width - new_size.width
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)

    assert top + bottom + new_size.height == desired_size.height, \
        "{},{},{}, does not add up to {}".format(str(top),
                                                 str(bottom),
                                                 str(old_size.height),
                                                 str(top + bottom +
                                                     old_size.height))
    assert left + right + new_size.width == desired_size.width
    return new_im



def transform_frame(frame: PicFrame, desired_size=DEFAULT_SIZE):
    # load img
    im = cv2.imread(frame.file_path)
    _im_shape = im.shape[:2]
    original_size = ImageSize(height=_im_shape[0], width=_im_shape[1])

    # resize accordingly (based on detected face size):
    _new_height, _new_width = tuple([int(x * frame.scale_ratio) for x in original_size])
    new_size = ImageSize(height=_new_height, width=_new_width)
    resized_img = cv2.resize(im, (new_size.width, new_size.height))

    # crop & zero padding:
    # face bbox centroid:
    _y = int((frame.reference_b_box_pre_scaled.y_min + frame.reference_b_box_pre_scaled.y_max) / 2 * frame.scale_ratio)
    _x = int((frame.reference_b_box_pre_scaled.x_min + frame.reference_b_box_pre_scaled.x_max) / 2 * frame.scale_ratio)
    assert 0 < _y < new_size.height
    assert 0 < _x < new_size.width

    # positive margin manes it needs padding, otherwise crop
    top_margin = (desired_size.height // 2 - _y)
    bottom_margin = ((desired_size.height - desired_size.height//2) - (new_size.height - _y))
    left_margin = (desired_size.width // 2 - _x)
    right_margin = ((desired_size.width - desired_size.width//2) - (new_size.width - _x))

    top, crop_top = max(0, top_margin), max(0, -top_margin)
    bottom, crop_bottom = max(0, bottom_margin), max(0, -bottom_margin)
    left, crop_left = max(0, left_margin), max(0, -left_margin)
    right, crop_right = max(0, right_margin), max(0, -right_margin)

    # crop if applies
    if crop_top>0 or crop_bottom>0 or crop_left>0 or crop_right>0:
        croped_img = resized_img[crop_top:(new_size.height-crop_bottom), crop_left:(new_size.width-crop_right)]
    else:
        croped_img = resized_img

    # print(croped_img.shape[0], new_size.height-crop_bottom-crop_top)
    assert croped_img.shape[0] == new_size.height-crop_bottom-crop_top
    assert croped_img.shape[1] == new_size.width-crop_left-crop_right

    # padding if applies:
    if top > 0 or bottom > 0 or left > 0 or right > 0:
        color = [0, 0, 0]
        padded_img = cv2.copyMakeBorder(croped_img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
    else:
        padded_img = croped_img

    final_img = padded_img
    return final_img



def write_video(codec, form, frame_list: [PicFrame], verbose=10, frame_rate=1):
    wkdir = os.path.dirname(os.path.dirname(__file__))

    # datetime object containing current date and time
    now = str(datetime.now())

    now = re.sub(':', '-', now)

    dir_head = os.path.join(wkdir, 'output')
    if not os.path.exists(dir_head):
        os.mkdir(dir_head)

    video_fp = os.path.join(dir_head, 'V_{}_{}.{}'.format(now, codec, form))

    out = cv2.VideoWriter(video_fp, cv2.VideoWriter_fourcc(*codec), frame_rate,
                          (DEFAULT_SIZE.width, DEFAULT_SIZE.height))

    i = 0

    for frame in frame_list:
        if i % verbose == 1:
            print(i, '/', len(frame_list))
        img_rs = transform_frame(frame=frame, desired_size=DEFAULT_SIZE)
        # img_rs = cv2.resize(cv2.imread(filename), (DEFAULT_SIZE.width, DEFAULT_SIZE.height))
        assert tuple(img_rs.shape[:2]) == tuple(DEFAULT_SIZE)
        out.write(img_rs)
    out.release()

    print('Export video at {}'.format(video_fp))

    print('Done with ', codec)



def write_video_sliding(codec, form, frame_list: [PicFrame],
                        verbose=100, frame_rate=1,
                        step_size=3, age_group='WTF', enable_win_slide=True, tag='A'):

    wkdir = os.path.dirname(os.path.dirname(__file__))

    # datetime object containing current date and time
    now = str(datetime.now())

    now = re.sub(':', '-', now)

    dir_head = os.path.join(wkdir, 'output_'+str(IDEAL_FACE_HEIGHT))
    if not os.path.exists(dir_head):
        os.mkdir(dir_head)

    video_fp = os.path.join(dir_head, '{}_{}_V_{}_{}.{}'.format(tag, age_group, now, codec, form))

    out = cv2.VideoWriter(video_fp, cv2.VideoWriter_fourcc(*codec), frame_rate,
                          (DEFAULT_SIZE.width, DEFAULT_SIZE.height))

    i = 0
    meta = dict()
    meta_label = list(map(lambda x: (x.group.age, x.group.gender), frame_list))
    _a, _g = list(zip(*meta_label))
    meta['meta_label'] = meta_label
    meta['age_distribution'] = Counter(_a)
    meta['gender_distribution'] = Counter(_g)
    meta['total'] = len(_a)

    with open(video_fp+'.json', 'w') as json_file:
        json.dump(meta, fp=json_file, indent=4)

    if enable_win_slide:
        frame_generator = SlidingFrameGenerator(frame_pic_list=frame_list,
                                                desired_size=DEFAULT_SIZE, step_size=step_size)
    else:
        frame_generator = EasyFrameGenerator(frame_pic_list=frame_list,
                                                desired_size=DEFAULT_SIZE, step_size=step_size)

    for frame_screen in frame_generator:
        if i % verbose == 1:
            print(i, '/')
        assert tuple(frame_screen.shape[:2]) == tuple(DEFAULT_SIZE)
        out.write(frame_screen)
    out.release()

    print('Export video at {}'.format(video_fp))

    print('Done with ', codec)


class SlidingFrameGenerator:
    def __init__(self, frame_pic_list: [PicFrame], desired_size=DEFAULT_SIZE,
                 step_size=5, initial_width=0.8):
        self.in_screen = []
        self.space_left = desired_size.width
        self.offset = 0

        self.desired_size = desired_size
        self.step_size = step_size
        self.initial_width = int(self.desired_size.width * initial_width)

        self._is_end = False
        self.frame_pic_list = [] + frame_pic_list

    def _get_sub_frame(self, frame: PicFrame, lr_side_padding=False):
        # load img
        desired_size = self.desired_size
        im = cv2.imread(frame.file_path)
        _im_shape = im.shape[:2]
        original_size = ImageSize(height=_im_shape[0], width=_im_shape[1])

        # resize accordingly (based on detected face size):
        _new_height, _new_width = tuple([int(x * frame.tf_pre_scale_ratio * frame.scale_ratio) for x in original_size])
        new_size = ImageSize(height=_new_height, width=_new_width)
        resized_img = cv2.resize(im, (new_size.width, new_size.height))

        # crop & zero padding:
        # face bbox centroid:
        _y = int((frame.reference_b_box_pre_scaled.y_min + frame.reference_b_box_pre_scaled.y_max) / 2 * frame.scale_ratio)
        _x = int((frame.reference_b_box_pre_scaled.x_min + frame.reference_b_box_pre_scaled.x_max) / 2 * frame.scale_ratio)
        # print(_y, new_size.height)
        assert 0 < _y < new_size.height
        assert 0 < _x < new_size.width

        # positive margin manes it needs padding, otherwise crop
        top_margin = (desired_size.height // 2 - _y)
        bottom_margin = ((desired_size.height - desired_size.height // 2) - (new_size.height - _y))
        left_margin = (desired_size.width // 2 - _x)
        right_margin = ((desired_size.width - desired_size.width // 2) - (new_size.width - _x))

        top, crop_top = max(0, top_margin), max(0, -top_margin)
        bottom, crop_bottom = max(0, bottom_margin), max(0, -bottom_margin)
        left, crop_left = max(0, left_margin), max(0, -left_margin)
        right, crop_right = max(0, right_margin), max(0, -right_margin)

        # crop if applies
        if crop_top > 0 or crop_bottom > 0 or crop_left > 0 or crop_right > 0:
            croped_img = resized_img[crop_top:(new_size.height - crop_bottom), crop_left:(new_size.width - crop_right)]
        else:
            croped_img = resized_img

        # print(croped_img.shape[0], new_size.height-crop_bottom-crop_top)
        assert croped_img.shape[0] == new_size.height - crop_bottom - crop_top
        assert croped_img.shape[1] == new_size.width - crop_left - crop_right

        # padding if applies:
        if top > 0 or bottom > 0 or left > 0 or right > 0:
            color = [0, 0, 0]
            if not lr_side_padding:
                left = right = 0
            padded_img = cv2.copyMakeBorder(croped_img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
        else:
            padded_img = croped_img

        final_img = padded_img
        return final_img

    def _get_black_scene(self, width):
        blank_image = np.zeros((self.desired_size.height, width, 3), np.uint8)
        return blank_image

    def _render(self):
        if self.in_screen:
            assert self.offset < self.in_screen[0].shape[1]
        _to_concat = []
        _space_left = copy.deepcopy(self.desired_size.width)

        for i, fa in enumerate(self.in_screen):
            if i == 0:
                _to_concat.append(fa[:,self.offset:])
                _space_left -= (fa.shape[1]-self.offset)
            else:
                _to_concat.append(fa[:, :_space_left])
                _space_left -= min(_space_left, fa.shape[1])

        assert len(_to_concat) == len(self.in_screen)

        assert sum(map(lambda x: x.shape[1], _to_concat)) == self.desired_size.width - _space_left
        if _space_left > 0:
            _to_concat.append(self._get_black_scene(width=_space_left))

        rendered = np.concatenate(tuple(_to_concat), axis=1)
        assert rendered.shape[0] == self.desired_size.height
        # print(rendered.shape[1], self.desired_size.width)
        assert rendered.shape[1] == self.desired_size.width
        return rendered


    def _update(self, step_size):
        """
        update:
            self.offset
            self.in_screen
            self.space_left

        :return:
        """
        _is_end = False
        self.offset += step_size

        if not self.in_screen:
            _is_end = True
            print('end>>>>>>>>>>>>>>>')

        elif self.offset >= self.in_screen[0].shape[1]:
            dropped_sub_frame = self.in_screen.pop(0)
            self.offset -= dropped_sub_frame.shape[1]
            self.space_left += dropped_sub_frame.shape[1]

        assert self.space_left <= self.desired_size.width

        return _is_end


    def __iter__(self):

        black_scene = self._get_black_scene(width=self.initial_width)
        self.offset = 0
        self.space_left -= black_scene.shape[1]
        self.in_screen.append(black_scene)

        while not self._is_end:
            # print('margin:', self.space_left + self.offset)

            while self.space_left + self.offset > 0 and self.frame_pic_list:
                assert self.space_left <= self.desired_size.width
                pic = self.frame_pic_list.pop(0)
                _sub_frame = self._get_sub_frame(pic)
                self.space_left -= _sub_frame.shape[1]
                self.in_screen.append(_sub_frame)

            # assert self.offset < self.in_screen[0].shape[1]

            screen = self._render()

            self._is_end = self._update(step_size=self.step_size)

            yield screen


class EasyFrameGenerator(SlidingFrameGenerator):

    def __iter__(self):

        while self.frame_pic_list:
            pic_f = self.frame_pic_list.pop(0)
            rendered = self._get_sub_frame(frame=pic_f, lr_side_padding=True)
            yield rendered









