Build face video data set

### Instructions

#### _Step zero. Set up environment with docker:_
Please check Docker-srs/README.md
Then you should be able to config the remote python interpreter

Download dependency:
```
git https://github.com/sfzhang15/FaceBoxes.git
```
`face_detector_x.py` will need that for detecting the faces and make sure face is 
aligned in the middle and have the consistent size.


#### _Step one. download image data set for demographics testing:_
[Face dataset](https://susanqq.github.io/UTKFace/)
Copy/upload those file into docker container
And then enter the docker container and unzip the file:
```
tar -xvzf part1.tar.gz
tar -xvzf part2.tar.gz
tar -xvzf part3.tar.gz
# OR
tar -xvzf *.tar.gz
``` 

#### fix openCV with tensorflow
Not enough time to put them in the docker..
```

apt-get update
apt-get install -y libsm6 libxext6 libxrender-dev
pip install opencv-python
```


#### _Step two. run script to generate the video file_
```
python3 main_gen.py
```

you can change `video_editing.py` to change the gamma/brightness,
lower resolution, crop, and add padding at edge.

### Appendix
```
Age group:
Corrected classification :
Young kids – Under 12
Teenagers – 15 - 20
Young adults – 25-32
Adult – 38-53
Senior – 60+

```

### download youtube faceDB if you need

```
curl -O http://wolftau:wtal997@www.cslab.openu.ac.il/download/wolftau/YouTubeFaces.tar.gz
curl -O http://www.cslab.openu.ac.il/download/wolftau/YouTubeFaces.tar.gz
```


In case you have more questions please send an email to Ruoding Tian:
ruodingt7@outlook.com

