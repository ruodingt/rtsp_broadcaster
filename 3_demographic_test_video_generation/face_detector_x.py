import os
from face_detector import FaceDetector
from PIL import Image, ImageDraw
from project_types import BBox

_wkdir = os.path.abspath('')
wkdir = os.path.dirname(_wkdir)

MODEL_PATH = os.path.join(wkdir, 'face_inference', 'ckpt', 'model.pb')


class ProjectFaceDetector:
    def __init__(self):
        self.project_face_detector = FaceDetector(MODEL_PATH,
                                                  gpu_memory_fraction=0.25,
                                                  visible_device_list='0')

    @staticmethod
    def draw_boxes_on_image(image, boxes, scores):
        image_copy = image.copy()
        draw = ImageDraw.Draw(image_copy, 'RGBA')
        width, height = image.size

        for b, s in zip(boxes, scores):
            ymin, xmin, ymax, xmax = b
            fill = (255, 0, 0, 45)
            outline = 'red'
            draw.rectangle(
                [(xmin, ymin), (xmax, ymax)],
                fill=fill, outline=outline
            )
            draw.text((xmin, ymin), text='{:.3f}'.format(s))
        return image_copy

    def get_b_box(self, image_array):
        boxes, scores = self.project_face_detector(image_array, score_threshold=0.3)
        return list(map(lambda b: BBox(b), boxes)), scores








if __name__ == '__main__':
    print(wkdir)