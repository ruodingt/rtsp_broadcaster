from config import CODEC_GROUP, N_PER_GROUP
from videofy2 import PictureFolderTree, PicRepo, PicFrame
from video_utils import write_video, write_video_sliding
from project_types import AgeGenderGroup

def get_final_img_path_list(selected: {str: [PicFrame]}) -> [str]:
    final_path_list = [] # : [str]
    for group_name, g_list in sorted(selected.items(), key=lambda k: k[0]):
        final_path_list += list(map(lambda k: k.file_path, g_list))

    return final_path_list


def get_final_list_of_pic_frame(selected: {str: [PicFrame]}) ->[PicFrame]:
    frame_list = []
    for group_name, g_list in sorted(selected.items(), key=lambda k: k[0]):
        frame_list += g_list
    return frame_list


if __name__ == '__main__':
    AGE_GROUP_TO_USE = AgeGenderGroup.YoungChildren

    pic_repo = PicRepo(pic_folder_tree=PictureFolderTree(), pipeline_test_size=0,
                       group_filter=lambda x: x.is_group(AGE_GROUP_TO_USE))
    selected_repo = pic_repo.get_best_n_samples_from_each_group(n=200) # : {str: [PicFrame]}
    # final_list_of_paths = get_final_img_path_list(selected_repo)
    final_list_frame = get_final_list_of_pic_frame(selected_repo)

    for c in CODEC_GROUP:
        for ff in ['mp4']:
            # write_video(codec=c, form=ff, frame_list=final_list_frame, frame_rate=10)
            write_video_sliding(codec=c, form=ff, frame_list=final_list_frame,
                                frame_rate=1, verbose=100, step_size=5,
                                age_group=AGE_GROUP_TO_USE, enable_win_slide=False,
                                tag='B')

    print('Done, done!')

