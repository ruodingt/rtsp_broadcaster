# Overview
please use Docker-tf to setup your environment
use remote interpreter provided by docker container

# Content Table

#### 0_Docker-tf
for setting up environment
#### 1_effective_installation
For effective installation analysis
#### 2_bap_evaluation
Evaluation BAP
#### 3_demographic_test_video_generation
Generate video data for eval demographics
#### 4_facescrub
Another dataset for testing BAP, optional. (Never used in our experiment


#### Questions?
Contact ruodingt7@outlook.com
