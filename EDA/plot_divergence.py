import numpy as np
import matplotlib.pyplot as plt

def _divergence(pk):
    qk = 1- pk
    _s = np.sum(pk * np.nan_to_num(np.log(pk / (qk + 1) + 1)), axis=0)
    _s2 = np.sum(qk * np.nan_to_num(np.log(qk / (pk + 1) + 1)), axis=0)
    return _s + _s2

x = np.arange()