import copy
import csv
import json
import os
import re
from collections import OrderedDict, Counter
from functools import reduce
import numpy as np


class TexTableMaker:
    def __init__(self, field_name):
        _fn = filter(lambda x : 'Time' not in x, field_name)
        self.field_name = list(_fn)

    def make_head(self):
        row = "\\begin{tabular}" + "{|"+ '|'.join(['c']*len(self.field_name)) +"|}" + "\n\hline\n"
        field_name = list(map(lambda x: re.sub('Age - ', '', x), self.field_name))
        row += ' & '.join(field_name) + r'\\' + '\n' + r'\hline' + '\n'
        return row

    @staticmethod
    def make_end():
        return "\\end{tabular}\n"

    def make_row(self, dic_row):
        elements = list(map(lambda x: str(dic_row[x]), self.field_name))
        row = ' & '.join(elements) + r'\\' + '\n' + r'\hline' + '\n'
        return row

    def make_multi_col(self, text, head_col=None):
        _cc = '{|c|}'
        _tx = '{' + str(text) + '}'
        if head_col is None:
            _lg = '{'+ str(len(self.field_name)) +'}'
            row = "\\multicolumn{}{}{}".format(_lg, _cc, _tx) + r'\\' + "\n\\hline\n"
        else:
            _lg = '{' + str(len(self.field_name)-1) + '}'
            row = "{}&\\multicolumn{}{}{}".format(head_col, _lg, _cc, _tx) + r'\\' + "\n\\hline\n"
        return row



class LatexReporter:
    def __init__(self, limit=10):
        _wkdir = os.path.abspath('')
        self.wkdir = os.path.dirname(_wkdir)
        self.out_dir = os.path.join(self.wkdir, 'output')
        self.age_group_name_map = {}
        self.limit = limit

    def intify(self, arr):
        # _arr = filter(lambda x: ':' not in x, arr)
        # print(_arr)
        return list(map(lambda x: int(x), arr))

    def intify_counter(self, ct):
        _cct = Counter()
        for k, v in ct.items():
            if ':' in v:
                pass
            elif v == '':
                _cct[k] = -1
            else:
                _cct[k] = int(v)

        return _cct

    def read_record(self, fp, head_col='*'):
        _zero = False
        _ri = -1 # record index
        _accumulator = Counter()
        record = []
        _sum = Counter()
        _mean = Counter()
        _fields = None

        with open(fp, mode='r',  encoding='utf-8') as report_file:
            reader = csv.DictReader(report_file)
            _fields = reader.fieldnames

            for _r in reader:
                _r0 = self.intify_counter(_r)
                r = Counter(_r0)

                if sum(self.intify(r.values())) == 0:
                    acc = sum(_accumulator.values())
                    if acc > 0:
                        record.append(_accumulator)
                        _accumulator = Counter()
                    _ri += 1
                elif sum(self.intify(r.values())) > 0 and _ri >= 0:
                    _accumulator += r
        record = record[:self.limit]
        _sum, _lg = reduce(lambda a, b :  a+b, record), len(record)
        _mean = copy.deepcopy(_sum)

        def _m_(a):
            _mean[a] /= _lg

        _ = list(map(lambda a: _m_(a), _sum.keys()))


        if head_col:
            _fields.insert(0, head_col)
            for i, r in enumerate(record):
                r[head_col] = i+1

            _sum[head_col] = 'Total'
            _mean[head_col] = 'Mean'

        return record, _sum, _mean, _fields

    def kullback_leibler_divergence(self, p, q, field_name):
        """
        Gives KLD(p|q) - forward KL
        :param p: Ground truth
        :param q:
        :return:
        """
        pk = np.array(list(map(lambda x: p[x], field_name[1:])))
        qk = np.array(list(map(lambda x: q[x], field_name[1:])))
        _s = np.sum(pk * np.nan_to_num(np.log(pk / (qk+1)+1)), axis=0) / 400
        _s2 = np.sum(qk * np.nan_to_num(np.log(qk / (pk+1)+1)), axis=0) / 400
        return _s + _s2

    def get_ground_truth(self, bn, field_name, head_col):
        # gth_dir = os.path.join(self.wkdir, 'data', 'test_truth')
        _base_dic = dict(zip(field_name, [0]*len(field_name)))
        _base_dic[head_col] = 'Ground Truth'

        _base_dic["Gender - Male"] = 200
        _base_dic["Gender - Female"] = 200

        gn = re.search('group_[0-9]', bn).group(0)
        if gn == "group_1":
            _base_dic["Age - Young Kids"] = 400
        elif gn == "group_2":
            _base_dic["Age - Teenagers"] = 400
        elif gn == "group_3":
            _base_dic["Age - Young Adults"] = 400
        elif gn == "group_4":
            _base_dic["Age - Adults"] = 400
        elif gn == "group_5":
            _base_dic["Age - Seniors"] = 400

        return _base_dic

    # gender_group_1_YK_0 - 12.csv
    def get_detail_table(self):
        for fp in os.listdir(os.path.join(self.wkdir, 'data', 'gorilla_table')):
            _bn = os.path.basename(fp)

            head_col = '*'

            _record, _s, _m, _fields = self.read_record(os.path.join(self.wkdir, 'data', 'gorilla_table', fp),
                                                        head_col=head_col)

            tex_table_maker = TexTableMaker(field_name=_fields)
            with open(os.path.join(self.out_dir, _bn+'.tex'), mode='w', encoding='utf-8') as tex_file:
                tex_file.write(tex_table_maker.make_head())
                for r in _record:
                    tex_file.write(tex_table_maker.make_row(r))
                tex_file.write(tex_table_maker.make_row(_s))
                tex_file.write(tex_table_maker.make_row(_m))

                ground_truth = self.get_ground_truth(_bn, field_name=_fields, head_col=head_col)
                tex_file.write(tex_table_maker.make_row(ground_truth))

                divergence = self.kullback_leibler_divergence(p=ground_truth, q=_m, field_name=_fields)

                tex_file.write(tex_table_maker.make_multi_col(divergence, head_col='KL Divergence'))

                tex_file.write(tex_table_maker.make_end())






if __name__ == '__main__':
    tex_report = LatexReporter()
    tex_report.get_detail_table()

